﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ComicApp.Models
{
    public class CharacterComicsVM
    {
        public int ComicId { get; set;}
        public int Character { get; set; }

    }
}