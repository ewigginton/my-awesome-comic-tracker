﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ComicApp.Models
{
    public class CharacterVM
    {
        public int CharacterId { get; set; }
        public int MarvelCharId { get; set; }
        public string ImageUrl { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string UserId { get; set; }
        

       
    }
}
