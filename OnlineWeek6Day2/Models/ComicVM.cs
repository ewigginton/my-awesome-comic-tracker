﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ComicApp.Models
{
    public class ComicVM
    {
        public int ComicId { get; set; }
        public int MarvelComicId { get; set; }
        public int IssueNumber { get; set; }
        public string Description { get; set; }
        public string Format { get; set; }
        public string ImageUrl { get; set; }
        public string Title { get; set; }
        public int SeriesId { get; set; }
        public string UserId { get; set; }
        //Nick
        //The error you're getting here says that Characters is less assessible than ComicVM is.
        //In a little more English it's basically saying I can't see or talk to CharacterVM.  What do we put in front of
        //everything thing we write in C# when we're defining stuff.  Classes, Properties, Methods, etc. that specifiy its access?
        public List<CharacterVM> Characters { get; set; }
        public bool IsDeleted { get; set; }

        public bool IsPhysical { get; set; }
        public bool IsDigital { get; set; }
    }
}