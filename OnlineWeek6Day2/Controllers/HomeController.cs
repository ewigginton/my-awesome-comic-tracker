﻿using ComicApp.Adapter;
using ComicApp.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ComicApp.Controllers
{
    public class HomeController : Controller
    {
        IComicAdapter _adapter;

        public HomeController()
        {
            _adapter = new ComicAdapter();
        }

        public HomeController(IComicAdapter adapter)
        {
            _adapter = adapter;
        }
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }
        public ActionResult Comics()
        {
            //Nick
            //When you fix your IComicAdapter to match ComicAdapter, this is going to give you a red underline
            //The first thing you'll want to do if you intend to use this is you'll need to move this to another
            //controller - an ApiController.  Remember, the HomeController is given to you when you start your project
            //and it links to an MVC HTML page which is not what you want.  You want to get the comics and return them
            //as JSON.  I see you have an apiUserController - you could create an apiComicsController and put this code in
            //the Get!  Finally, in a factory in AngularJS you can make your $http call to your apiComics controller to grab
            //all your comics from your SQL database.
            
            //You'll again have to match what it is asking for, too!  It will be missing a parameter but you may want to change
            //the adapter and interface to not even want a parameter if you're just trying to get ALL comics.
            var model = _adapter.GetAllComics("asdf");

            return View(model);
        }
    }
}
