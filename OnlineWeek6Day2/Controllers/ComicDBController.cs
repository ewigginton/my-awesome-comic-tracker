﻿using ComicApp.Adapters.Adapters;
using ComicApp.Adapters.Interfaces;
using ComicApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Net.Http;
using Microsoft.AspNet.Identity;
using ComicApp.Data;
using ComicApp.Data.Model;
using ComicApp.Adapter;

namespace ComicApp.Controllers
{
    public class ComicDBController : ApiController 
    {
        private IComicAdapter _adapter;

        public ComicDBController()
        {
            _adapter = new ComicAdapter();
        }

        public ComicDBController(ComicAdapter a)
        {
            _adapter = a;
        }
        [Authorize]
        public IHttpActionResult Get()
        {
            string UserId = User.Identity.GetUserId();
            List<Comic> comics;
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                comics = db.Comics.Where(x => x.IsDeleted == false && x.UserId == UserId).ToList();
            }
            return Ok(comics);
        }

      /*  public IHttpActionResult Get(int ComicId)
        {

            Comic comic;
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                comic = db.Comics.Find(ComicId);
            }
            return Ok(comic);
        }*/
       
        [Authorize]

        public IHttpActionResult Post(Comic comic)
        {
            string UserId = User.Identity.GetUserId();
            if (UserId == null) return BadRequest("You Must Be Logged In to Save Comics");

            else { 
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    comic.UserId = UserId;
                    db.Comics.Add(comic);
                    db.SaveChanges();
                }
                return Ok();
            }
        }
        
        [Authorize]
        public IHttpActionResult Put(Comic comic)
        {
            string UserId = User.Identity.GetUserId();
            if (UserId == null) return BadRequest("You Must Be Logged In to Edit");

            else if (comic.UserId == UserId || User.IsInRole("Admin"))
            {

                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var currentComic = db.Comics.Find(comic.ComicId);
                    currentComic.IsDigital = comic.IsDigital;
                    currentComic.IsPhysical = comic.IsPhysical;
                    db.SaveChanges();
                }
                return Ok();
            }

            else return BadRequest("Insufficient privileges");
        }
    }
}