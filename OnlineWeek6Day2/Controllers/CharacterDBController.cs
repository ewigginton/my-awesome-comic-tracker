﻿using ComicApp.Adapters.Adapters;
using ComicApp.Adapters.Interfaces;
using ComicApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Net.Http;
using Microsoft.AspNet.Identity;
using ComicApp.Data;
using ComicApp.Data.Model;

namespace ComicApp.Controllers
{
    public class CharacterDBController : ApiController
    {
            private ICharacterAdapter _adapter;

            public CharacterDBController()
            {
                _adapter = new CharacterAdapter();

            }

            public  CharacterDBController(CharacterAdapter a)
            {
                _adapter = a;
            }

            [Authorize]
            public IHttpActionResult Get()
            {
                    string UserId = User.Identity.GetUserId();
                    //TODO: Add (x => x.UserId == UserId)to the following after working with user accounts

                    List<Character> characters;
                    using (ApplicationDbContext db = new ApplicationDbContext())
                    {
                        characters = db.Characters.Where(x => x.UserId == UserId).ToList();
                    }
                    return Ok(characters);
            }

            
           /* public IHttpActionResult Get(int CharacterId)
            {
                //string UserId = User.Identity.GetUserId();
                //TODO: Add (x => x.UserId == UserId)to the following after working with user accounts

                Character character;
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    character = db.Characters.Find(CharacterId);
                }
                return Ok(character);
            }*/

            
            [Authorize]
            public IHttpActionResult Post(Character character)
            {
                string UserId = User.Identity.GetUserId();
                        using (ApplicationDbContext db = new ApplicationDbContext())
                        {
                            character.UserId = UserId;
                            db.Characters.Add(character);
                            db.SaveChanges();

                        }
                        return Ok();
            }

            [Authorize]

            public IHttpActionResult PUT(Character character)
            {
                 string UserId = User.Identity.GetUserId();
                 if (UserId == null) return BadRequest("You Must Be Logged In to Save Roster");

                 else
                 {
                     using (ApplicationDbContext db = new ApplicationDbContext())
                     {
                         var currentCharacter = db.Characters.Find(character.CharacterId);
                         currentCharacter.Name = character.Name;
                         // currentCharacter.DateUpdated = DateTime.Now;
                         currentCharacter.Description = character.Description;
                         db.SaveChanges();
                     }
                     return Ok();
                 }
            }

            [Authorize]

            public IHttpActionResult Delete(int CharacterId)
            {
                string UserId = User.Identity.GetUserId();
                if (UserId == null) return BadRequest("You Must Be Logged In to Save Roster");

                else
                {
                    using (ApplicationDbContext db = new ApplicationDbContext())
                    {
                        var currentCharacter = db.Characters.Find(CharacterId);
                        db.Characters.Remove(currentCharacter);
                        db.SaveChanges();
                    }
                    return Ok();
                }
            }

        }
    }

