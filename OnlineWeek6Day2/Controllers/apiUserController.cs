﻿using Microsoft.AspNet.Identity; //for User.Identity.GetUserId();
using ComicApp.Adapters.Adapters;
using ComicApp.Adapters.Interfaces;
using ComicApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ComicApp.Controllers
{
    public class apiUserController : ApiController
    {
        private IUserAdapter _adapter;

        public apiUserController()
        {
            _adapter = new UserAdapter();
        }

        public apiUserController(IUserAdapter a)
        {
            _adapter = a;
        }

        [Authorize]
        public IHttpActionResult Get()
        {
            string UserId = User.Identity.GetUserId();
            UserVM user = _adapter.GetUserInfo(UserId);
            return Ok(user);
        }

        public IHttpActionResult Post(RegisterUserVM model)
        {
            string result = _adapter.RegisterUser(model);
            return Ok(result);
        }
    }
}