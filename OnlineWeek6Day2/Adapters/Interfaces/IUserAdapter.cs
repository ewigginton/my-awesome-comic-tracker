﻿using ComicApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComicApp.Adapters.Interfaces
{
    public interface IUserAdapter
    {
        string RegisterUser(RegisterUserVM user);

        UserVM GetUserInfo(string UserId);
    }
}