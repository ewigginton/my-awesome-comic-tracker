﻿using ComicApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComicApp.Adapters.Interfaces
{
    public interface ICharacterAdapter
    {
        List<CharacterVM> GetAllCharacters(string id);

        CharacterVM GetCharacter(int? id);

        CharacterVM AddCharacter(CharacterVM model);

      

       
    }
}
