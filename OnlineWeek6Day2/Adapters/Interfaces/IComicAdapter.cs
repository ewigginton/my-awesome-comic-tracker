﻿using ComicApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComicApp.Adapter
{
    public interface IComicAdapter
    {
        //Nick
        //This MUST match what's in ComiderAdapter.cs
        //ComicAdapter.cs: List<ComicVM> GetAllComics(string Email)
        //IComicAdapter.cs: ComicVM GetAllComics()
        //You see the difference?  Make sure this one below is the same as what ComicAdapter.cs has for the method name,
        //parameters, and the return type
        List<ComicVM> GetAllComics(string UserId);

        ComicVM AddComic(ComicVM model);

        ComicVM UpdateComic(ComicVM model);


    }
}
