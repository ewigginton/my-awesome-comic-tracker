﻿using ComicApp.Adapters.Interfaces;
using ComicApp.Data;
using ComicApp.Data.Model;
using ComicApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ComicApp.Adapters.Adapters
{
    public class CharacterAdapter : ICharacterAdapter
    {
        

        
        
        public CharacterVM GetCharacter(int? id)
        {
            CharacterVM model;
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                model = db.Characters.Where(x => x.CharacterId == id && x.IsDeleted == false).Select(x => new CharacterVM()
                {
                    Name = x.Name,
                    Description = x.Description,
                    ImageUrl = x.ImageUrl,
                    CharacterId = x.CharacterId,
                }).FirstOrDefault();
            }
            return model;
        }

        public List<CharacterVM> GetAllCharacters(string UserId)
        {
            List<CharacterVM> model;
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                model = db.Characters.Where(x => x.UserId == UserId && x.IsDeleted == false).Select(x => new CharacterVM()
                    {
                        Name = x.Name,
                        CharacterId = x.CharacterId,
                        ImageUrl = x.ImageUrl,
                        Description = x.Description,
                    }).ToList();
            }
            return model;
        }


        public CharacterVM AddCharacter(CharacterVM model)
        {
            throw new NotImplementedException();
        }
    }

   
}
