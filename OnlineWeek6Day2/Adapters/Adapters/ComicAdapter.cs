﻿using ComicApp.Data;
using ComicApp.Data.Model;
using ComicApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ComicApp.Adapter
{
    public class ComicAdapter : IComicAdapter
    {
        //Nick
        //This is complaining that there isn't anything being returned but you're saying you want to return List<ComicVM>
        //Of course you do want to return a list of comics!  You're missing 'return model;' at the bottom of your method to
        //indicate this is what you want to return that matches what you said you want in the method signature.
        public List<ComicVM> GetAllComics(string UserId)
        {
            List<ComicVM> model;

             using (ApplicationDbContext db = new ApplicationDbContext())
             {
                 string userId = db.Users.Where(x => x.Email == UserId).FirstOrDefault().Id;

                 if (userId == null) return new List<ComicVM>();

                 model = db.Comics.Where(x => x.UserId == UserId).Select(x => new ComicVM()
                     {
                         Format = x.Format,
                         IssueNumber = x.IssueNumber,
                         Description = x.Description,
                         ImageUrl = x.ImageUrl,
                         Title = x.Title,
                         IsDeleted = x.IsDeleted,
                     }).ToList();
             }
             return model;
          
         }

        public ComicVM AddComic(ComicVM model)
        {
            Comic newComic = new Comic();
            newComic.Title = model.Title;
            newComic.IssueNumber = model.IssueNumber;
            newComic.IsDeleted = false;
            newComic.ImageUrl = model.ImageUrl;
            newComic.IsPhysical = false;
            newComic.IsDigital = false;
            newComic.ComicId = model.ComicId;
            newComic.UserId = model.UserId;
            newComic.Description = model.Description;
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                db.Comics.Add(newComic);
                db.SaveChanges();
            }

            return GetComic(newComic.ComicId);
        }

        public ComicVM GetComic(int ComicId)
        {
            ComicVM model;

            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                model = db.Comics.Where(x => x.ComicId == ComicId && x.IsDeleted == false).Select(x => new ComicVM()
                {
                    Format = x.Format,
                    IssueNumber = x.IssueNumber,
                    Description = x.Description,
                    ImageUrl = x.ImageUrl,
                    Title = x.Title,
                    Characters = db.CharacterComics.Where(y => y.ComicId == x.ComicId).Select(z => new CharacterVM()
                         {
                             CharacterId = z.CharacterId,
                             Name = z.Character.Name
                         }).ToList()
                }).FirstOrDefault();
            }
            return model;

        }



        public ComicVM UpdateComic(ComicVM model)
        {
            throw new NotImplementedException();
        }
    }
    }
