﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using ComicApp.Adapters.Interfaces;
using ComicApp.Data;
using ComicApp.Data.Model;
using ComicApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ComicApp.Adapters.Adapters
{
    public class UserAdapter : IUserAdapter
    {
        public string RegisterUser(RegisterUserVM user)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
                if (db.Users.Any(x => x.UserName == user.Username))
                {
                    return "A user already exists with that username.";
                }
                ApplicationUser newUser = new ApplicationUser()
                {
                    UserName = user.Username,
                    Email = user.Email
                };
                manager.Create(newUser, user.Password);
                manager.AddToRole(newUser.Id, "User");
            }
            return "User has been successfully registered.";
        }

        public UserVM GetUserInfo(string UserId)
        {
            UserVM model;
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                model = db.Users.Where(x => x.Id == UserId).Select(x => new UserVM()
                {
                    Email = x.Email,
                    Username = x.UserName
                }).FirstOrDefault();
            }
            return model;
        }
    }
}