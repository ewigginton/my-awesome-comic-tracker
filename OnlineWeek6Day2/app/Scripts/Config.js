﻿function Config($routeProvider) {
    $routeProvider.when('/', {
        templateUrl: '/app/Views/Home.html',
        title: 'Home Page',
        controller: 'MarvelCharacterApiController'
    }).when('/Register', {
        templateUrl: '/app/Views/Register.html',
        title: 'Register',
        controller: 'HomeController'
    }).when('/AccountManagement', {
        templateUrl: '/app/Views/AccountManagement.html',
        title: 'Manage Account'
    }).when('/View/:id', {
        templateUrl: '/app/Views.html',
        title: 'Comic Info'
    }).when('/Collection', {
        templateUrl: '/app/Views/Collection.html',
        controller: 'CollectionController'
    }).when('/Edit/:id', {
        templateUrl: '/app/View/Edit.html',
        title: 'Edit Information'  
    }).when('/ShowComic', {
        templateUrl: 'app/Views/ShowComic.html',
        title: 'Comic Information',
        controller: 'ComicBookController'
    }).when('/Characters', {
        templateUrl: 'app/Views/Characters.html',
        title: 'Character Roster',
        controller: 'CharacterController'
    }).otherwise({
        redirectTo: '/'
    })
}

Config.$inject = ['$routeProvider'];