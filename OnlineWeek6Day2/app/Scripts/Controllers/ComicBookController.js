﻿app.controller('ComicBookController', function ($location, $http, $scope, MarvelApiFactory) {
    $scope.singleComic = MarvelApiFactory.getComicBook();
  
    $scope.addComic = function (singleComic) {

        var newComic = {
            Title: singleComic.title,
            MarvelCharId: singleComic.id,
            ImageUrl: singleComic.thumbnail.path + '.' + singleComic.thumbnail.extension,
            IssueNumber: singleComic.issueNumber,
            Description: singleComic.description,
            Format: singleComic.format,
            IsDeleted: false
            //TODO: Fix This!   DateCreated: DateTime.Now,            
        }

        return MarvelApiFactory.addNewComic(newComic).then(function (data) {
        }

       // $scope.getAllCharacters
        )
    }
})