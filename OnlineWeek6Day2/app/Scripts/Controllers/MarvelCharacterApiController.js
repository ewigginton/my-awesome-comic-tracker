﻿app.controller('MarvelCharacterApiController', function ($location, $http, $scope, MarvelApiFactory) {

    $scope.searchResult = {};
    $scope.hasSearched = false;
    $scope.showSeries = false;
    $scope.showComics = false;

    $scope.displayCharacter = function () {
        //Starting search, hide stuff
        $scope.showSeries = false;
        $scope.showComics = false;

        return MarvelApiFactory.getApiCharacterId($scope.Search).then(function (data) {

            $scope.hasSearched = true;
            $scope.searchResult = data;
        }, function (data) { })
    }

    $scope.displayComics = function () {
        $scope.showComics = true;

    }

    $scope.displaySeries = function () {
        $scope.showSeries = true;
    }

    $scope.hideSeries = function () {
        $scope.showSeries = false;
    }

    $scope.hideComics = function () {
        $scope.showComics = false;
    }

    //TODO: Got this working. Now I need to figure out how to change pages. I did a ng-click + href. 
    //The API calls, but it doesn't change pages. I'm working on that now. 


    $scope.getComicInfo = function (input) {
        return MarvelApiFactory.getSingleComic(input).then(function (data) {
            console.log(input);
            MarvelApiFactory.setComicBook(data);
            $location.path('/ShowComic');
        }, function (data) { })
    }
    
    // Database functions     
 
  


    $scope.addCharacter = function (searchResults) {

        var newCharacter = {
            Name: searchResults.name,
            MarvelCharId: searchResults.id, 
            ImageUrl: searchResults.thumbnail.path + '.' + searchResults.thumbnail.extension,
            Description: searchResults.description,
            IsDeleted: false
         //TODO: Fix This!   DateCreated: DateTime.Now,            
        }

        return MarvelApiFactory.addNewCharacter(newCharacter).then(function (data) {
        }

       // $scope.getAllCharacters
        )}

})
            
   /*  Character newCharacter = new Character();
        
        using (ApplicationDbContext db = new ApplicationDbContext())
        {
            db.Characters.Add(newCharacter);
            db.SaveChanges();
        }
        return GetCharacter(newCharacter.CharacterId);
        }

   return MarvelApiFactory.getApiCharacterId(input).then(function (allCharacters) {
        $scope.newCharacter = allCharacters[6].results;
        console.log($scope.newCharacter);
    }, function (data) { })
}

$scope.addComic = function (input) {
    return MarvelApiFactory.getApiCharacterId(input).then(function (allCharacters) {
        $scope.newCharacter = allCharacters[6].results;
        console.log($scope.newCharacter);
    }, function (data) { })
}*/

   