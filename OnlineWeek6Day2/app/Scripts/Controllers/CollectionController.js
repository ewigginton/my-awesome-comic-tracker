﻿app.controller('CollectionController', function ($location, $http, $scope, MarvelApiFactory) {

    $scope.displayAllComics = function () {
        return MarvelApiFactory.getAllComics().then(function (allComics) {
            console.log(allComics);
            $scope.allComics = allComics;
           
        }, function (data) { })
    }

    $scope.delete = false;

    $scope.physical = false;

    $scope.digital = false;

    $scope.editing = false;

    $scope.physicalComic = function() {
        if ($scope.physical) {
            $scope.physical = false;
        }
        else
            $scope.physical = true;

    }

    $scope.digitalComic = function () {
        if ($scope.digital) {
            $scope.digital = false;
        }
        else {
            $scope.digital = true;
        }

    }


    $scope.updateComic = function (ComicId) {
        var comic = {
            ComicId: ComicId,
            IsPhysical: $scope.physical,
            IsDigital: $scope.digital,
            }

        return MarvelApiFactory.editComic(comic).then(function (data) {
            })
    }

    $scope.displayAllComics();
})

