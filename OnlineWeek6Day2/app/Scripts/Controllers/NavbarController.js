﻿function NavbarController($scope, LoginFactory, UserFactory, $location) {
    $scope.status = LoginFactory.status;
    $scope.user = {
        username: '',
        password: ''
    }
    $scope.newUser = {
        username: '', email: '', password: '', confirmPassword: ''
    }
    $scope.Login = function () {
        LoginFactory.login($scope.user).then(function () {
            $scope.user.username = ''; $scope.user.password = '';
        }, function () {
            $scope.user.password = '';
            alert("Invalid username/password combination");
        })
    }
    $scope.LogOut = function () {
        LoginFactory.logout();
    }
    $scope.registerUser = function () {
        if ($scope.newUser.password === $scope.newUser.confirmPassword && $scope.newUser.password) {
            UserFactory.register({
                username: $scope.newUser.username,
                email: $scope.newUser.email,
                password: $scope.newUser.password
            }).then(function (data) {
                console.log(data);
                $location.path('/')
            }, function (data) {
                console.log(data);
            })
        }
        else {
            $scope.newUser.password = $scope.newUser.confirmPassword = '';
            alert("Your passwords must match");
        }
    }
    $scope.getUser = function () {
        UserFactory.getUser().then(function (data) {
            $scope.editUser = data;
        });
    }
    // Stops the dropdown from closing when an input inside of it is clicked
    angular.element('.dropdown-menu').bind('click', function (e) { e.stopPropagation(); });
}
NavbarController.$inject = ['$scope', 'LoginFactory', 'UserFactory', '$location'];