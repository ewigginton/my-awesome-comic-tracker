﻿app.factory("MarvelApiFactory", function($http, $q) {



   var allComics = [];
   var allSeries = [];
   var allCharacters = [];
   

    var pubKey = "ea80bf9125178142d39a588fcb39dbf8";
    var priKey = "b2e611d7c8e626a85efbc8fe3bd3564781c58e24";
    var apiUrl = "http://gateway.marvel.com:80/v1/public/";
    var comic = {};
    // Get Single Character from Marvel


    var getApiCharacterId = function (input) {
        
        var str = apiUrl + "characters?name=" + input + "&apikey=" + pubKey;
        var deferred = $q.defer();

        $http({
            url: str,
            method: "GET"
        }).success(function (data) {
            var searchResult = data.data.results[0];
            deferred.resolve(searchResult);
        }).error(function(data){
            deferred.reject(data);
        })


        return deferred.promise;
    }

    //Get Single Comic Book from Marvel
    var setComicBook = function (singleComic) {
        comic = singleComic;
    }

    var getComicBook = function () {
        return comic;
    }

    var getSingleComic = function (input) {

        var str = input + "%20?apikey=" + pubKey;
        var deferred = $q.defer();

        $http({
            url: str,
            method: "GET"
        }).success(function (data) {
            var singleComic = data.data.results[0];
            deferred.resolve(singleComic);
        }).error(function (data) {
            deferred.reject(data);
        });


        return deferred.promise;
    };
  

    //Database calls

      var addNewCharacter = function (newCharacter) {
        
        var deferred = $q.defer();

        $http({
            url: '/api/CharacterDB',
            method: "POST",
            headers: { Authorization: "Bearer " + localStorage.getItem('token') },
            data: newCharacter
        }).success(function () {
            deferred.resolve();
        }).error(function(){
            deferred.reject();
        })
          return deferred.promise;
      }

      var getAllCharacters = function () {
          var deferred = $q.defer();

          $http({
              url: '/api/CharacterDB',
              method: 'GET',
              headers: { Authorization: "Bearer " + localStorage.getItem('token') }
          }).success(function (data) {
              for (var i = 0; i < data.length; i++) {
                  allCharacters.push(data[i]);
              }
              deferred.resolve(data);
          }).error(function (data) {
              console.log("there was a problem retriving allCharacters");
              deferred.reject(data);
          });

          return deferred.promise;
      }

      var addNewComic = function (newComic) {

          var deferred = $q.defer();

          $http({
              url: '/api/ComicDB',
              method: "POST",
              headers: { Authorization: "Bearer " + localStorage.getItem('token') },
              data: newComic
          }).success(function () {
              deferred.resolve();
          }).error(function () {
              deferred.reject();
          })
          return deferred.promise;
      }

      var getAllComics = function (input) {
          var deferred = $q.defer();

          $http({
              url: '/api/ComicDB',
              headers: { Authorization: "Bearer " + localStorage.getItem('token') },
              method: 'GET'
          }).success(function (data) {
              for (var i = 0; i < data.length; i++) {
                  allComics.push(data[i]);
              }
              deferred.resolve(data);
          }).error(function (data) {
              console.log("there was a problem retriving allComics");
              deferred.reject(data);
          });

          return deferred.promise;
      }

      var editComic = function (comic) {

          var deferred = $q.defer();

          $http({
              url: '/api/ComicDB',
              method: "PUT",
              headers: { Authorization: "Bearer " + localStorage.getItem('token') },
              data: comic
          }).success(function () {
              deferred.resolve();
          }).error(function () {
              deferred.reject();
          })
          return deferred.promise;
      }


      var removeThing = function (id) {

          var deferred = $q.defer();

          $http({
              url: '/api/ComicDB',
              method: "DELETE",
              headers: { Authorization: "Bearer " + localStorage.getItem('token') },
              data: id
          }).success(function () {
              deferred.resolve();
          }).error(function () {
              deferred.reject();
          })
          return deferred.promise;
      }



    return {
        addNewComic: addNewComic,
        getApiCharacterId: getApiCharacterId,
        addNewCharacter: addNewCharacter,
        getAllCharacters: getAllCharacters,
        allCharacters: allCharacters,
        getComicBook: getComicBook,
        setComicBook: setComicBook,
        getSingleComic: getSingleComic,
        getAllComics: getAllComics,
        editComic: editComic,
        removeThing: removeThing
   
    }
})