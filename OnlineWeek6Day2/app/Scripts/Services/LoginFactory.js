﻿function LoginFactory($http, $q) {
    var status = {

    }

    if (localStorage.getItem('token')) status.loggedIn = true;
    else status.loggedIn = false;

    var login = function (user) {
        var def = $q.defer();

        $http({
            url: '/Token',
            method: 'POST',
            contentType: 'application/x-www-form-urlencoded',
            data: 'username=' + user.username + '&password=' + user.password + '&grant_type=password'
        }).success(function (data) {
            localStorage.setItem('token', data.access_token);
            status.loggedIn = true;
            def.resolve();
        }).error(function (data) {
            status.loggedIn = false;
            localStorage.removeItem('token');
            def.reject();
        });
        return def.promise;
    }

    var logout = function () {
        localStorage.removeItem('token');
        status.loggedIn = false;
    }

    return {
        login: login,
        status: status,
        logout: logout
    }
}

LoginFactory.$inject = ['$http', '$q'];