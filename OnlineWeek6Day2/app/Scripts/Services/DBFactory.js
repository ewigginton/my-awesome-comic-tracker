﻿app.factory("MarvelApiFactory", function($http, $q) {
    var getCharacters = [];
    var addCharacters = [];

    var AddCharacter = function (data) {
        var def = $q.defer();
        $http({
            url: '/api/CharacterController',
            headers: { Authorization: "Bearer " + localStorage.getItem('token') },
            method: 'PUT',
            data: data
        }).success(function (data) {
            recipe = angular.copy(data);
            def.resolve();
        }).error(function (data) {
            console.log("There was an error.");
            def.reject();
        });
        return def.promise;
    };

    var getAllCharacters = function () {
        var def = $q.defer();
        $http({
            url: '/api/CharacterController/',
            method: 'GET'
        }).success(function (data) {
            //clearArray();
            for (var i = 0; i < data.length; i++) {
                getCharacters.push(data[i]);
            }

            def.resolve();
        }).error(function (data) {
            console.log("There was an error.");
            def.reject();
        });
        return def.promise;
    };

    return {
        getCharacters: getCharacters,
        addChracters: addCharacters

    }
})