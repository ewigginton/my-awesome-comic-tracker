﻿function UserFactory($http, $q) {
    var register = function (user) {
        var def = $q.defer();
        $http({
            url: '/api/apiUser',
            method: 'POST',
            contentType: 'application/json',
            data: user
        }).success(function (data) {
            def.resolve(data);
        }).error(function (data) {
            def.reject(data);
        });
        return def.promise;
    }
    var getUser = function () {
        var def = $q.defer();
        $http({
            url: '/api/apiUser',
            method: 'GET',
            headers: { Authorization: "Bearer " + localStorage.getItem('token') }
        }).success(function (data) {
            def.resolve(data);
        }).error(function (data) {
            def.reject(data);
        });
        return def.promise;
    }

    return {
        register: register,
        getUser: getUser
    }
}

UserFactory.$inject = ['$http', '$q'];