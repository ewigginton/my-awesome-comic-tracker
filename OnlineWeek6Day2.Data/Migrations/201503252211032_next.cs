namespace ComicApp.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class next : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Comics", "SeriesId", "dbo.Series");
            DropIndex("dbo.Comics", new[] { "SeriesId" });
            RenameColumn(table: "dbo.Comics", name: "SeriesId", newName: "Series_SeriesId");
            AlterColumn("dbo.Comics", "Series_SeriesId", c => c.Int());
            CreateIndex("dbo.Comics", "Series_SeriesId");
            AddForeignKey("dbo.Comics", "Series_SeriesId", "dbo.Series", "SeriesId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Comics", "Series_SeriesId", "dbo.Series");
            DropIndex("dbo.Comics", new[] { "Series_SeriesId" });
            AlterColumn("dbo.Comics", "Series_SeriesId", c => c.Int(nullable: false));
            RenameColumn(table: "dbo.Comics", name: "Series_SeriesId", newName: "SeriesId");
            CreateIndex("dbo.Comics", "SeriesId");
            AddForeignKey("dbo.Comics", "SeriesId", "dbo.Series", "SeriesId", cascadeDelete: true);
        }
    }
}
