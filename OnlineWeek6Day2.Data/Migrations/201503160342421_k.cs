namespace ComicApp.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class k : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CharacterComics", "CharacterId", c => c.Int(nullable: false));
            AddColumn("dbo.CharacterComics", "ComicId", c => c.Int(nullable: false));
            CreateIndex("dbo.CharacterComics", "CharacterId");
            CreateIndex("dbo.CharacterComics", "ComicId");
            AddForeignKey("dbo.CharacterComics", "CharacterId", "dbo.Characters", "CharacterId", cascadeDelete: true);
            AddForeignKey("dbo.CharacterComics", "ComicId", "dbo.Comics", "ComicId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CharacterComics", "ComicId", "dbo.Comics");
            DropForeignKey("dbo.CharacterComics", "CharacterId", "dbo.Characters");
            DropIndex("dbo.CharacterComics", new[] { "ComicId" });
            DropIndex("dbo.CharacterComics", new[] { "CharacterId" });
            DropColumn("dbo.CharacterComics", "ComicId");
            DropColumn("dbo.CharacterComics", "CharacterId");
        }
    }
}
