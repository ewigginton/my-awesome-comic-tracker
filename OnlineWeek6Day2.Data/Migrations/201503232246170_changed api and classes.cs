namespace ComicApp.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changedapiandclasses : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Characters", "MarvelCharId", c => c.Int(nullable: false));
            AddColumn("dbo.Comics", "MarvelComicId", c => c.Int(nullable: false));
            AddColumn("dbo.Series", "MarvelSeriesId", c => c.Int(nullable: false));
            DropColumn("dbo.Characters", "Creator");
            DropColumn("dbo.Characters", "RealName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Characters", "RealName", c => c.String());
            AddColumn("dbo.Characters", "Creator", c => c.String());
            DropColumn("dbo.Series", "MarvelSeriesId");
            DropColumn("dbo.Comics", "MarvelComicId");
            DropColumn("dbo.Characters", "MarvelCharId");
        }
    }
}
