namespace ComicApp.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class asdfs : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Characters", "Creator", c => c.String());
            AddColumn("dbo.Characters", "UserId", c => c.String(maxLength: 128));
            AddColumn("dbo.Comics", "Title", c => c.String());
            CreateIndex("dbo.Characters", "UserId");
            AddForeignKey("dbo.Characters", "UserId", "dbo.AspNetUsers", "Id");
            DropColumn("dbo.Characters", "Publisher");
            DropColumn("dbo.Comics", "Name");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Comics", "Name", c => c.String());
            AddColumn("dbo.Characters", "Publisher", c => c.String());
            DropForeignKey("dbo.Characters", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.Characters", new[] { "UserId" });
            DropColumn("dbo.Comics", "Title");
            DropColumn("dbo.Characters", "UserId");
            DropColumn("dbo.Characters", "Creator");
        }
    }
}
