﻿using Microsoft.AspNet.Identity.EntityFramework;
using ComicApp.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ComicApp.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Comic> Comics { get; set; }
        public DbSet<CharacterComic> CharacterComics { get; set; }
        public DbSet<Series> Serieses { get; set; }
        public DbSet<Collection> Collections { get; set; }
        public DbSet<Wishlist> Wishlists { get; set; }
        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Character> Characters { get; set; }




        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
            this.Configuration.LazyLoadingEnabled = false;
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}