﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using ComicApp.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Migrations;

namespace ComicApp.Data
{
    public static class Seeder
    {
        public static void Seed(ApplicationDbContext 
            db, bool roles = true, bool users = true, bool comics = true, bool charCom = true, bool series = true, bool collections = true, bool wishlists = true, bool blogs = true, bool categories = true, bool characters = true)
        {
            if (roles) SeedRoles(db);
            if (users) SeedUsers(db);
            if (comics) SeedComics(db);
            if (charCom) SeedCharCom(db);
            if (series) SeedSeries(db);
           
            if (characters) SeedCharacters(db);

            
            
           // if (collections) SeedCollections(db);
           // if (wishlists) SeedWishlists(db);
            //if (blogs) SeedBlogs(db);
            
           // if (categories) SeedCategories(db);
        }

        private static void SeedCharCom(ApplicationDbContext db)
        {
            db.CharacterComics.AddOrUpdate(x => x.CharacterComicId,
                new CharacterComic() {CharacterId = 18, ComicId = 6},
               new CharacterComic() {CharacterId = 19, ComicId = 6}
                );
        }

    

        private static void SeedComics(ApplicationDbContext db)
        {
            db.Comics.AddOrUpdate(x => x.ComicId,
                new Comic() {Title = "Series Name", Format = "Hardcover", ImageUrl = "pic goes here", Description = "Superfriends save the day", IssueNumber=3}
                );
        }

        private static void SeedSeries(ApplicationDbContext db)
        {
            string userId = db.Users.Where(x => x.UserName == "test").FirstOrDefault().Id;
            db.Serieses.AddOrUpdate(x => x.SeriesId,
                new Series() {Name = "Spider-Man Does Stuff",  Description="Spider goes on an adventure", UserId = userId}
                );
        }

        private static void SeedCharacters(ApplicationDbContext db)
        {
            db.Characters.AddOrUpdate(x => x.Name,
                new Character() {Description = "Description goes here", Name = "Spider-Man", ImageUrl = "Picture goes here",},
                new Character() {Description = "Description goes here", Name = "Red Falcon", ImageUrl = "Picture goes here",}
                );
        }

       
       

        

        private static void SeedRoles(ApplicationDbContext db)
        {
            var manager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));

            if (!manager.RoleExists("User"))
            {
                manager.Create(new IdentityRole() { Name = "User" });
            }
            if (!manager.RoleExists("Admin"))
            {
                manager.Create(new IdentityRole() { Name = "Admin" });
            }
        }

        private static void SeedUsers(ApplicationDbContext db)
        {
            var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));

            if (!db.Users.Any(x => x.Email == "test@test.com"))
            {
                ApplicationUser user = new ApplicationUser()
                {
                    UserName = "test",
                    Email = "test@test.com",

                };
                manager.Create(user, "123123");
                manager.AddToRole(user.Id, "User");
            }
            if (!db.Users.Any(x => x.UserName == "Admin"))
            {
                ApplicationUser user = new ApplicationUser()
                {
                    UserName = "Admin",
                    Email = "admin@test.com"
                };
                manager.Create(user, "123123");
                manager.AddToRole(user.Id, "Admin");
            }
        }

        
    }
}