﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComicApp.Data.Model
{
    public class Comic
    {
        [Key]
        public int ComicId { get; set; }
        public int MarvelComicId { get; set; }
        public int IssueNumber { get; set; }
        public string Description { get; set; }
        public string Format { get; set; }
        public string ImageUrl { get; set; }
        public string Title { get; set; }
        public List<Character> Characters { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime? DateUpdated { get; set; }

        public bool IsPhysical { get; set; }
        public bool IsDigital { get; set; }
        public string UserId { get; set; }

       public virtual ApplicationUser User { get; set; }

       public Comic()
            {
                IsDeleted = false;
                DateCreated = DateTime.Now;
                IsDigital = false;
                IsPhysical = false;

            }

    }
}
