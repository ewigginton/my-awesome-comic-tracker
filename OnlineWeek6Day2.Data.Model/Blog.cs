﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComicApp.Data.Model
{
    public class Blog
    {
        [Key]
        public int PostId { get; set; }
        public string PostTitle { get; set; }
        public DateTime PostTime { get; set; }
        public string PostBody { get; set; }
        public virtual List<Category> Categories { get; set; }
        public bool IsDeleted { get; set; }

            public DateTime DateCreated { get; set; }

            public DateTime? DateUpdated { get; set; }

            public string UserId { get; set; }

            public virtual ApplicationUser User { get; set; }

            public Blog()
            {
                IsDeleted = false;
                DateCreated = DateTime.Now;
                UserId = "admin";

            }

    }
}
