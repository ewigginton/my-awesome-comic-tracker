﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComicApp.Data.Model
{
    public class Character
    {
        [Key]
        public int CharacterId { get; set; }
        public int MarvelCharId { get; set; }
        public string ImageUrl { get; set; }

        public string Name { get; set; }
       
        public string Description { get; set; }
        public string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }


        public bool IsDeleted { get; set; }

            public DateTime DateCreated { get; set; }

            public DateTime? DateUpdated { get; set; }

            public Character()
            {
                IsDeleted = false;
                DateCreated = DateTime.Now;

            }      
    }
}


