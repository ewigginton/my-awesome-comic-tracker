﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComicApp.Data.Model
{
    public class Series 
    {
        [Key]
        public int SeriesId { get; set; }
        public int MarvelSeriesId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }


        public virtual List<Comic> Comics { get; set; }

        public bool IsDeleted { get; set; }

            public DateTime DateCreated { get; set; }

            public DateTime? DateUpdated { get; set; }

            public string UserId { get; set; }

            public virtual ApplicationUser User { get; set; }

            public Series()
            {
                IsDeleted = false;
                DateCreated = DateTime.Now;

            }

    }
}
