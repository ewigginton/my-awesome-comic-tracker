﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComicApp.Data.Model
{
    public class Wishlist 
    {
        [Key]
        public int WishlistId { get; set; }
        public string Name { get; set;}
        public string Notes { get; set; }
        public virtual List<Comic> Comics { get; set; }
        public virtual List<Series> Serieses { get; set; }
       
        public bool IsDeleted { get; set; }

            public DateTime DateCreated { get; set; }

            public DateTime? DateUpdated { get; set; }
            public string UserId { get; set; }

            public virtual ApplicationUser User { get; set; }

            public Wishlist()
            {
                IsDeleted = false;
                DateCreated = DateTime.Now;

            }
    }
}
