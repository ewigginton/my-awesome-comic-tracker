﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComicApp.Data.Model
{
    public class CharacterComic
    {
        [Key]
        public int CharacterComicId { get; set; }
        public int CharacterId { get; set; }
        public virtual Character Character { get; set; }
        public int ComicId { get; set; }
        public virtual Comic Comic { get; set; }

    }
}
